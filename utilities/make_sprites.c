// Copyright (C) 2021 Jean-Noël Vittaut aka Obscoz

// This file is part of Super-Mario-Bros-IIc.

// Super-Mario-Bros-IIc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Super-Mario-Bros-IIc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Super-Mario-Bros-IIc.  If not, see <http://www.gnu.org/licenses/>

#include <stdint.h>
#include <stdio.h>

uint32_t rol(uint32_t x, uint32_t trans, int p) {
  if (x == trans) {
    return 0;
  }
  x = (x * 0x0202020202ULL & 0x010884422010ULL) % 1023;
  x = x >> 4; // bits reversed
  x <<= (p * 3) & 0b11;
  return ((x & 0b11110000) >> 4) | (x & 0b00001111);
}

int main() {
  uint8_t bytes[5][12][128] = { 0 };

  char buffer[64];
  uint32_t tx[4];
  int tx_nr;
  while (scanf("%x %s", &tx_nr, buffer) != EOF) {
    uint32_t transp;
    scanf("%x", &transp);
    for (int t = 0; t < 3; ++t) {
      scanf("%x %x %x %x", tx, tx + 1, tx + 2, tx + 3);
      for (int i = 0; i < 4; ++i) {
        for (int p = 0; p < 4; ++p) {
          uint32_t tex = tx[i];
          uint8_t *byt = &bytes[p][4 * t + i][tx_nr];
          uint32_t a = rol((tex & 0x0f000000) >> 24, transp, p);
          uint32_t b = rol((tex & 0x00f00000) >> 20, transp, p);
          uint32_t c = rol((tex & 0x000f0000) >> 16, transp, p);
          uint32_t d = rol((tex & 0x0000f000) >> 12, transp, p);
          uint32_t e = rol((tex & 0x00000f00) >> 8, transp, p);
          uint32_t f = rol((tex & 0x000000f0) >> 4, transp, p);
          uint32_t g = rol((tex & 0x0000000f) >> 0, transp, p);
          byt[0] = (b << 4 | a) & 0x7f;
          byt[2] = (d << 5 | c << 1 | b >> 3) & 0x7f;
          byt[1] = (f << 6 | e << 2 | d >> 2) & 0x7f;
          byt[3] = (         g << 3 | f >> 1) & 0x7f;
        }
        {
          uint32_t tex = tx[i];
          uint8_t *byt = &bytes[4][4 * t + i][tx_nr];
          uint32_t a = (tex & 0x0f000000) >> 24 == transp ? 0xf : 0;
          uint32_t b = (tex & 0x00f00000) >> 20 == transp ? 0xf : 0;
          uint32_t c = (tex & 0x000f0000) >> 16 == transp ? 0xf : 0;
          uint32_t d = (tex & 0x0000f000) >> 12 == transp ? 0xf : 0;
          uint32_t e = (tex & 0x00000f00) >> 8 == transp ? 0xf : 0;
          uint32_t f = (tex & 0x000000f0) >> 4 == transp ? 0xf : 0;
          uint32_t g = (tex & 0x0000000f) >> 0 == transp ? 0xf : 0;
          byt[0] = (b << 4 | a) & 0x7f;
          byt[2] = (d << 5 | c << 1 | b >> 3) & 0x7f;
          byt[1] = (f << 6 | e << 2 | d >> 2) & 0x7f;
          byt[3] = (         g << 3 | f >> 1) & 0x7f;

        }
      }
    }
  }
  for (int p = 0; p < 5; ++p) {
    for (int l = 0; l < 12; ++l) {
      int k;
      for (k = 0; k < 128; ++k) {
        putchar(bytes[p][l][k]);
      }
    }
  }
  return 0;
}

