#include <stdio.h>
#include <stdint.h>

uint8_t high[3][16];
uint8_t low[3][16];

int main() {
  for (int t = 0; t < 192 / 4; ++t) {
    int l = 4 * t + 4;
    uint16_t addr = -984 * (l / 64) + 128 * (l / 8) + (l % 8) * 1024 + 2;
    high[t % 3][t / 3] = addr >> 8;
    low[t % 3][t / 3] = addr & 0xff;
  }

  for (int i = 0; i < 3; ++i) {
    printf("vl_low%d:\n", i);
    for (int j = 0; j < 16; ++j) {
      printf("  .byte $%x\n", low[i][j]);
    }
  }
  for (int i = 0; i < 3; ++i) {
    printf("vl_high%d:\n", i);
    for (int j = 0; j < 16; ++j) {
      printf("  .byte $%x\n", high[i][j]);
    }
  }
  return 0;
}

