// Copyright (C) 2021 Jean-Noël Vittaut aka Obscoz

// This file is part of Super-Mario-Bros-IIc.

// Super-Mario-Bros-IIc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Super-Mario-Bros-IIc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Super-Mario-Bros-IIc.  If not, see <http://www.gnu.org/licenses/>

#include <stdint.h>
#include <stdio.h>
#include <assert.h>

void process_line(uint32_t tiles[15]) {
  for(int l = 0; l < 15; ++l) {
    if (tiles[14 - l] != 0xff) {
      putchar(17 * l + 16);
      putchar(tiles[14 - l]);
    }
  }
  putchar(0);
}

int main() {
  int i = 0;
  uint32_t tiles[15];
  while (scanf("%x", tiles + i) != EOF) {
    i = (i + 1) % 15;
    if (i == 0) {
      process_line(tiles);
    }
  }
  return 0;
}

