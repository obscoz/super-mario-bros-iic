// Copyright (C) 2021 Jean-Noël Vittaut aka Obscoz

// This file is part of Super-Mario-Bros-IIc.

// Super-Mario-Bros-IIc is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Super-Mario-Bros-IIc is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Super-Mario-Bros-IIc.  If not, see <http://www.gnu.org/licenses/>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t disk[35][16][256] = { 0 };
uint8_t used[35][16] = { 0 };

int main() {
  for(int tr = 0; tr < 35; ++tr) {
    for (int s = 0; s < 16; ++s) {
      for (int b = 0; b < 256; ++b) {
        disk[tr][s][b] = (tr << 4) | s;
      }
    }
  }


  while(getchar() != '\n');

  char filename[256];
  int nb_sectors;
  while (scanf("%s %d", filename, &nb_sectors) != EOF) {
    FILE *binary = fopen(filename, "r");
    if (! binary) {
      fprintf(stderr, "Cannot open '%s'\n", filename);
      exit(1);
    }
    fseek(binary, 0, SEEK_END);
    int filelen = ftell(binary);
    rewind(binary);
    if (filelen > 256 * nb_sectors) {
      fprintf(stderr, "Not enough sectors to store %s\n", filename);
      exit(1);
    }
    if (filelen <= 256 * (nb_sectors - 1)) {
      fprintf(stderr, "Warning: Too many sectors allocated to %s\n", filename);
    }
    for (int i = 0; i < nb_sectors; ++i) {
      int track, sector;
      scanf(" [%d, %d]", &track, &sector);
      if(used[track][sector]) {
        fprintf(stderr, "Double use of track %d, sector %d\n", track, sector);
        exit(1);
      }
      fread(disk[track][sector], 256, 1, binary);
      used[track][sector] = 1;
    }
    fclose(binary);
  }

  int nb_read = fwrite(disk, 256, 35 * 16, stdout);
  if (nb_read != 35 * 16) {
    fprintf(stderr, "Error while writing disk image\n");
    exit(1);
  }
  return 0;
}

