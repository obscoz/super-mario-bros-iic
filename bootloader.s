; Copyright (C) 2021 Jean-Noël Vittaut aka Obscoz
;
; This file is part of Super-Mario-Bros-IIc.
;
; Super-Mario-Bros-IIc is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Super-Mario-Bros-IIc is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Super-Mario-Bros-IIc.  If not, see <http://www.gnu.org/licenses/>

.setcpu   "65c02"

.segment  "CODE"

.include "smc.inc"
.include "non-free/mini_rwts.inc"
.include "mem_locations.inc"

; track 00, sector 00
              .org    $800
              .byte   04              ; number of sectors to read

; ----------------------------------------------------------------------------
; main
; ----------------------------------------------------------------------------
.proc main
              txa                     ; acc=slot*16 (bits 7 to 4)
              ora     #%00000001      ; set drive=1
              sta     PARM_SD         ; slot/drive
              lda     #0              ; we're on track $00
              sta     ZCURTRK

              stz     RAMRW_D000B1    ; disable ROM

              jsr     move_rtws       ; move RWTS

;              lda     #>reset         ; set the reset routine
;              sta     ADR_RESET
;              lda     #<reset
;              sta     ADR_RESET + 1

              jmp     reset

cold_start:   jsr     load_mario
              jsr     load_textures
              jsr     load_sprites
              jsr     load_level1
              jmp     MARIO

reset:        ldx     #$ff
              txs
              jmp     cold_start
.endproc ; main


; ----------------------------------------------------------------------------
; load_mario
; ----------------------------------------------------------------------------
.proc load_mario
              lda     #<MARIO
              sta     PTR_BUFFL
              lda     #>MARIO
              sta     PTR_BUFFH
              lda     #01
              sta     TRACK
              ldx     #06
              stx     SECTOR
              ldy     #07
              sty     NB_SECT
              jmp     MINI_RWTS
.endproc ; load_mario


; ----------------------------------------------------------------------------
; load_textures
; ----------------------------------------------------------------------------
.proc load_textures
              lda     #<TEXTURES
              sta     PTR_BUFFL
              lda     #>TEXTURES
              sta     PTR_BUFFH
              lda     #02
              sta     TRACK
              ldx     #15
              stx     SECTOR
              ldy     #16
              sty     NB_SECT
              jsr     MINI_RWTS
              lda     #>TEXTURES + $10
              sta     PTR_BUFFH
              lda     #03
              sta     TRACK
              ldx     #15
              stx     SECTOR
              ldy     #8
              sty     NB_SECT
              jmp     MINI_RWTS
.endproc ; load_textures


; ----------------------------------------------------------------------------
; load_sprites
; ----------------------------------------------------------------------------
.proc load_sprites
              lda     #<SPRITES
              sta     PTR_BUFFL
              lda     #>SPRITES
              sta     PTR_BUFFH
              lda     #04
              sta     TRACK
              ldx     #15
              stx     SECTOR
              ldy     #16
              sty     NB_SECT
              jsr     MINI_RWTS
              lda     #>SPRITES + $10
              sta     PTR_BUFFH
              lda     #05
              sta     TRACK
              ldx     #15
              stx     SECTOR
              ldy     #14
              sty     NB_SECT
              jmp     MINI_RWTS
.endproc ; load_textures


; ----------------------------------------------------------------------------
; load_level1
; ----------------------------------------------------------------------------
.proc load_level1
              lda     #<LEVEL
              sta     PTR_BUFFL
              lda     #>LEVEL
              sta     PTR_BUFFH
              lda     #03
              sta     TRACK
              ldx     #07
              stx     SECTOR
              ldy     #08
              sty     NB_SECT
              jmp     MINI_RWTS
.endproc ; load_level1


; ----------------------------------------------------------------------------
; move_rtws
; ----------------------------------------------------------------------------
.proc move_rtws
              lda     #$09
              sta     ZPTR1 + 1
              lda     #>MINI_RWTS
              sta     ZPTR2 + 1
              lda     #$00
              sta     ZPTR1
              sta     ZPTR2
              ldx     #$03

next_page:    ldy     #$00

next_byte:    lda     (ZPTR1), Y
              sta     (ZPTR2), Y
              iny
              bne     next_byte

              inc     ZPTR1 + 1
              inc     ZPTR2 + 1
              dex
              bne     next_page

              rts
.endproc ; move_rtws

