; Copyright (C) 2021 Jean-Noël Vittaut aka Obscoz
;
; This file is part of Super-Mario-Bros-IIc.
;
; Super-Mario-Bros-IIc is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Super-Mario-Bros-IIc is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Super-Mario-Bros-IIc.  If not, see <http://www.gnu.org/licenses/>

.setcpu     "65c02"

.include "smc.inc"
.include "non-free/mini_rwts.inc"
.include "mem_locations.inc"

MAGIC_BYTE    :=      $f3

.segment  "CODE"

              .org    MARIO

; ----------------------------------------------------------------------------
; main
; ----------------------------------------------------------------------------
.proc main
              jsr     enable_vbl
              jsr     clear_dhgr
              jsr     dhgr
              jsr     copy_sprites
              ldx     #2
render2pages: phx
              jsr     clear_text
              jsr     init_level
              jsr     render
              ldy     #15
draw_one:     phy
              jsr     shift_left
              jsr     shift_left
              jsr     shift_left
              jsr     shift_left
              jsr     render
              ply
next:         dey
              bpl     draw_one
              jsr     switch_display
              plx
              dex
              bne     render2pages
scroll:       jsr     process_input
              jsr     render
              jsr     render_mario
              jsr     switch_display
              bra     scroll
.endproc ; main

; ----------------------------------------------------------------------------
; enable_vbl
; ----------------------------------------------------------------------------
.proc enable_vbl
              sei                     ; disable interrupts
              stz     IOUON
              stz     VBLINTON
              stz     IOUOFF
              lda     VSYNCIRF
              rts
.endproc


; ----------------------------------------------------------------------------
; process_input
; ----------------------------------------------------------------------------
.proc process_input
              bit     KBD             ; is a new key pressed?
              bpl     go              ; no?
              bit     KBDSTRB         ; is the key still being pressed?
              SMC_LoadValue go
              eor     #$01
              SMC_StoreValue go
go:           SMC go, { lda #$00 }
              beq     exit
              jsr     shift_left
exit:         rts
.endproc


; ----------------------------------------------------------------------------
; dhgr
; ----------------------------------------------------------------------------
.proc dhgr
              stz     TXTCLR          ; display graphics
              stz     HIRES           ; Hires graphics
              stz     MIXCLR          ; disable 4 lines of text
              stz     DHIRESON        ; Enable double-width graphics
              stz     SET80MODE       ; turn on 80 columns
              stz     CLR80COL        ; turn off 80STORE
              rts
.endproc


; ----------------------------------------------------------------------------
; copy_to_aux
; in: page ZPTR1
; ----------------------------------------------------------------------------
.proc copy_to_aux
              ldy     #$00
loop:         lda     (ZPTR1), Y
              sta     (ZPTR1), Y
              iny
              bne     loop
              rts
.endproc


; ----------------------------------------------------------------------------
; clear_page
; in: char A, page ZPTR1
; ----------------------------------------------------------------------------
.proc clear_page
              ldy     #$00
loop:         sta     (ZPTR1), Y
              iny
              bne     loop
              rts
.endproc


; ----------------------------------------------------------------------------
; clear_text
; ----------------------------------------------------------------------------
.proc clear_text
              lda     #$ff
              stz     ZPTR1
              ldx     #>SCENE
clear:        stx     ZPTR1 + 1
              jsr     clear_page
              inx
              cpx     #>SCENE + 8
              bne     clear
              rts
.endproc


; ----------------------------------------------------------------------------
; copy_sprites
; ----------------------------------------------------------------------------
.proc copy_sprites
              stz     RAMWRTON
              lda     #$00
              stz     ZPTR1
              ldx     #>SPRITES
clear:        stx     ZPTR1 + 1
              jsr     copy_to_aux
              inx
              cpx     #>SPRITES + 30
              bne     clear
              stz     RAMWRTOFF
              rts
.endproc


; ----------------------------------------------------------------------------
; clear_dhgr
; ----------------------------------------------------------------------------
.proc clear_dhgr
              stz     RAMWRTON        ; 4c  turn on RAMWRT
              jsr     clear_one
              stz     RAMWRTOFF       ; 4c  turn off RAMWRT
              jsr     clear_one
              rts
clear_one:    stz     ZPTR1
              ldx     #$20
              lda     #$00
clear:        stx     ZPTR1 + 1
              jsr     clear_page
              inx
              cpx     #$60
              bne     clear
              rts
.endproc


; ----------------------------------------------------------------------------
; init_level
; ----------------------------------------------------------------------------
.proc init_level
              lda     #>LEVEL
              sta     ZLEVELPOS + 1
              lda     #<LEVEL
              sta     ZLEVELPOS
              lda     #0
              sta     ZPIXELSHIFT
              ldx     #16
loop:         lda     #00
              sta     SCENE, X
              txa
              clc
              adc     #17
              tax
              cpx     #15
              bne     loop
              rts
.endproc


; ----------------------------------------------------------------------------
; shift_left
; ----------------------------------------------------------------------------
.proc shift_left
              lda     ZPIXELSHIFT
              bne     update_psh
              ldx     #0
next_line:    ldy     #0
shift:        lda     SCENE + 1, X
              sta     SCENE, X
              inx
              iny
              cpy     #16
              bne     shift
              lda     SCENE, X
              bmi     transp
              beq     zero
              lda     #$01
zero:         dec
              sta     SCENE, X
transp:       inx
              cpx     #255
              bne     next_line

load_level:   ldy     #$00
new_column:   lda     (ZLEVELPOS), Y
              beq     update_lpos
              tax
              iny
              lda     (ZLEVELPOS), Y
              sta     SCENE, X
              iny
              bra     new_column
update_lpos:  tya
              inc
              clc
              adc     ZLEVELPOS
              sta     ZLEVELPOS
              lda     #$00
              adc     ZLEVELPOS + 1
              sta     ZLEVELPOS + 1
update_psh:   lda     ZPIXELSHIFT
              inc
              and     #%00000011
              sta     ZPIXELSHIFT
              rts
.endproc


; ----------------------------------------------------------------------------
; switch_display
; ----------------------------------------------------------------------------
.proc switch_display
              lda     page_shown
              eor     #<(LOWSCR ^ HISCR)
              sta     page_shown
              SMC_StoreLowByte p_shown
:             lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
              bpl     :-              ;   IIe : wait for VBLANK start
              lda     VSYNCIRF        ; IIc : reset VSYNC IRF
:             lda     VERTBLANK       ; IIe : wait for VBLANK end
              bmi     :-
:             lda     VERTBLANK       ; IIc : wait for VSYNC IRQ
              bpl     :-              ;   IIe : wait for VBLANK start
              SMC p_shown, { stz LOWSCR }
              rts
.endproc


; ----------------------------------------------------------------------------
; draw_tile
; ----------------------------------------------------------------------------
.proc draw_tile
              bcc     no_shift
shift:        jsr     draw
last_3_s:     inx
              jsr     draw
              stz     RAMWRTON
              jsr     draw
              inx
              jsr     draw
              stz     RAMWRTOFF
              rts
no_shift:     stz     RAMWRTON
              jsr     draw
              inx
              jsr     draw
              stz     RAMWRTOFF
              dex
              jsr     draw
              inx
draw:         SMC tex_0, { lda TEXTURES, Y }
              SMC scr_0, { sta SMC_AbsAdr, X }
              SMC tex_1, { lda TEXTURES + $80, Y }
              SMC scr_1, { sta SMC_AbsAdr, X }
              SMC tex_2, { lda TEXTURES, Y }
              SMC scr_2, { sta SMC_AbsAdr, X }
              SMC tex_3, { lda TEXTURES + $80, Y }
              SMC scr_3, { sta SMC_AbsAdr, X }
              SMC tex_4, { lda TEXTURES, Y }
              SMC scr_4, { sta SMC_AbsAdr, X }
              SMC tex_5, { lda TEXTURES + $80, Y }
              SMC scr_5, { sta SMC_AbsAdr, X }
              SMC tex_6, { lda TEXTURES, Y }
              SMC scr_6, { sta SMC_AbsAdr, X }
              SMC tex_7, { lda TEXTURES + $80, Y }
              SMC scr_7, { sta SMC_AbsAdr, X }
              SMC tex_8, { lda TEXTURES, Y }
              SMC scr_8, { sta SMC_AbsAdr, X }
              SMC tex_9, { lda TEXTURES + $80, Y }
              SMC scr_9, { sta SMC_AbsAdr, X }
              SMC tex_a, { lda TEXTURES, Y }
              SMC scr_a, { sta SMC_AbsAdr, X }
              SMC tex_b, { lda TEXTURES + $80, Y }
              SMC scr_b, { sta SMC_AbsAdr, X }
              iny
              rts
.endproc


; ----------------------------------------------------------------------------
; draw_sprite
; ----------------------------------------------------------------------------
.proc draw_sprite
              bcc     no_shift
shift:        jsr     draw
last_3_s:     inx
              jsr     draw
              stz     RAMWRTON
              stz     RAMRDON
              jsr     draw
              inx
              jsr     draw
              stz     RAMRDOFF
              stz     RAMWRTOFF
              rts
no_shift:     stz     RAMRDON
              stz     RAMWRTON
              jsr     draw
              inx
              jsr     draw
              stz     RAMRDOFF
              stz     RAMWRTOFF
              dex
              jsr     draw
              inx
draw:         SMC scrr_0, { lda SMC_AbsAdr, X }
              and SPRITES + $1800, Y
              SMC tex_0, { ora SPRITES, Y }
              SMC scrw_0, { sta SMC_AbsAdr, X }
              SMC scrr_1, { lda SMC_AbsAdr, X }
              and SPRITES + $1880, Y
              SMC tex_1, { ora SPRITES + $80, Y }
              SMC scrw_1, { sta SMC_AbsAdr, X }
              SMC scrr_2, { lda SMC_AbsAdr, X }
              and SPRITES + $1900, Y
              SMC tex_2, { ora SPRITES, Y }
              SMC scrw_2, { sta SMC_AbsAdr, X }
              SMC scrr_3, { lda SMC_AbsAdr, X }
              and SPRITES + $1980, Y
              SMC tex_3, { ora SPRITES + $80, Y }
              SMC scrw_3, { sta SMC_AbsAdr, X }
              SMC scrr_4, { lda SMC_AbsAdr, X }
              and SPRITES + $1a00, Y
              SMC tex_4, { ora SPRITES, Y }
              SMC scrw_4, { sta SMC_AbsAdr, X }
              SMC scrr_5, { lda SMC_AbsAdr, X }
              and SPRITES + $1a80, Y
              SMC tex_5, { ora SPRITES + $80, Y }
              SMC scrw_5, { sta SMC_AbsAdr, X }
              SMC scrr_6, { lda SMC_AbsAdr, X }
              and SPRITES + $1b00, Y
              SMC tex_6, { ora SPRITES, Y }
              SMC scrw_6, { sta SMC_AbsAdr, X }
              SMC scrr_7, { lda SMC_AbsAdr, X }
              and SPRITES + $1b80, Y
              SMC tex_7, { ora SPRITES + $80, Y }
              SMC scrw_7, { sta SMC_AbsAdr, X }
              SMC scrr_8, { lda SMC_AbsAdr, X }
              and SPRITES + $1c00, Y
              SMC tex_8, { ora SPRITES, Y }
              SMC scrw_8, { sta SMC_AbsAdr, X }
              SMC scrr_9, { lda SMC_AbsAdr, X }
              and SPRITES + $1c80, Y
              SMC tex_9, { ora SPRITES + $80, Y }
              SMC scrw_9, { sta SMC_AbsAdr, X }
              SMC scrr_a, { lda SMC_AbsAdr, X }
              and SPRITES + $1d00, Y
              SMC tex_a, { ora SPRITES, Y }
              SMC scrw_a, { sta SMC_AbsAdr, X }
              SMC scrr_b, { lda SMC_AbsAdr, X }
              and SPRITES + $1d80, Y
              SMC tex_b, { ora SPRITES + $80, Y }
              SMC scrw_b, { sta SMC_AbsAdr, X }
              iny
              rts
.endproc


; ----------------------------------------------------------------------------
; set_texture_set
; ----------------------------------------------------------------------------
.proc set_texture_set
              SMC_StoreHighByte draw_tile::tex_0
              SMC_StoreHighByte draw_tile::tex_1
              inc
              SMC_StoreHighByte draw_tile::tex_2
              SMC_StoreHighByte draw_tile::tex_3
              inc
              SMC_StoreHighByte draw_tile::tex_4
              SMC_StoreHighByte draw_tile::tex_5
              inc
              SMC_StoreHighByte draw_tile::tex_6
              SMC_StoreHighByte draw_tile::tex_7
              inc
              SMC_StoreHighByte draw_tile::tex_8
              SMC_StoreHighByte draw_tile::tex_9
              inc
              SMC_StoreHighByte draw_tile::tex_a
              SMC_StoreHighByte draw_tile::tex_b
              rts
.endproc


; ----------------------------------------------------------------------------
; set_sprite_set
; ----------------------------------------------------------------------------
.proc set_sprite_set
              SMC_StoreHighByte draw_sprite::tex_0
              SMC_StoreHighByte draw_sprite::tex_1
              inc
              SMC_StoreHighByte draw_sprite::tex_2
              SMC_StoreHighByte draw_sprite::tex_3
              inc
              SMC_StoreHighByte draw_sprite::tex_4
              SMC_StoreHighByte draw_sprite::tex_5
              inc
              SMC_StoreHighByte draw_sprite::tex_6
              SMC_StoreHighByte draw_sprite::tex_7
              inc
              SMC_StoreHighByte draw_sprite::tex_8
              SMC_StoreHighByte draw_sprite::tex_9
              inc
              SMC_StoreHighByte draw_sprite::tex_a
              SMC_StoreHighByte draw_sprite::tex_b
              rts
.endproc


; ----------------------------------------------------------------------------
; set_video_line
; ----------------------------------------------------------------------------
.proc set_video_line
              clc
              lda     vl_low0, Y
              adc     pixel_shift, X
              SMC_StoreLowByte draw_tile::scr_0
              SMC_StoreLowByte draw_tile::scr_1
              SMC_StoreLowByte draw_tile::scr_2
              SMC_StoreLowByte draw_tile::scr_3
              lda     vl_low1, Y
              adc     pixel_shift, X
              SMC_StoreLowByte draw_tile::scr_4
              SMC_StoreLowByte draw_tile::scr_5
              SMC_StoreLowByte draw_tile::scr_6
              SMC_StoreLowByte draw_tile::scr_7
              lda     vl_low2, Y
              adc     pixel_shift, X
              SMC_StoreLowByte draw_tile::scr_8
              SMC_StoreLowByte draw_tile::scr_9
              SMC_StoreLowByte draw_tile::scr_a
              SMC_StoreLowByte draw_tile::scr_b

              lda     vl_high0, Y
              adc     ZPAGE
              SMC_StoreHighByte draw_tile::scr_0
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_1
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_2
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_3
              lda     vl_high1, Y
              adc     ZPAGE
              SMC_StoreHighByte draw_tile::scr_4
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_5
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_6
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_7
              lda     vl_high2, Y
              adc     ZPAGE
              SMC_StoreHighByte draw_tile::scr_8
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_9
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_a
              adc     #$04
              SMC_StoreHighByte draw_tile::scr_b
              rts
.endproc


; ----------------------------------------------------------------------------
; set_sprite_line
; ----------------------------------------------------------------------------
.proc set_sprite_line
              clc
              lda     vl_low0, Y
              adc     pixel_shift, X
              SMC_StoreLowByte draw_sprite::scrr_0
              SMC_StoreLowByte draw_sprite::scrr_1
              SMC_StoreLowByte draw_sprite::scrr_2
              SMC_StoreLowByte draw_sprite::scrr_3
              SMC_StoreLowByte draw_sprite::scrw_0
              SMC_StoreLowByte draw_sprite::scrw_1
              SMC_StoreLowByte draw_sprite::scrw_2
              SMC_StoreLowByte draw_sprite::scrw_3
              lda     vl_low1, Y
              adc     pixel_shift, X
              SMC_StoreLowByte draw_sprite::scrr_4
              SMC_StoreLowByte draw_sprite::scrr_5
              SMC_StoreLowByte draw_sprite::scrr_6
              SMC_StoreLowByte draw_sprite::scrr_7
              SMC_StoreLowByte draw_sprite::scrw_4
              SMC_StoreLowByte draw_sprite::scrw_5
              SMC_StoreLowByte draw_sprite::scrw_6
              SMC_StoreLowByte draw_sprite::scrw_7
              lda     vl_low2, Y
              adc     pixel_shift, X
              SMC_StoreLowByte draw_sprite::scrr_8
              SMC_StoreLowByte draw_sprite::scrr_9
              SMC_StoreLowByte draw_sprite::scrr_a
              SMC_StoreLowByte draw_sprite::scrr_b
              SMC_StoreLowByte draw_sprite::scrw_8
              SMC_StoreLowByte draw_sprite::scrw_9
              SMC_StoreLowByte draw_sprite::scrw_a
              SMC_StoreLowByte draw_sprite::scrw_b

              lda     vl_high0, Y
              adc     ZPAGE
              SMC_StoreHighByte draw_sprite::scrr_0
              SMC_StoreHighByte draw_sprite::scrw_0
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_1
              SMC_StoreHighByte draw_sprite::scrw_1
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_2
              SMC_StoreHighByte draw_sprite::scrw_2
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_3
              SMC_StoreHighByte draw_sprite::scrw_3
              lda     vl_high1, Y
              adc     ZPAGE
              SMC_StoreHighByte draw_sprite::scrr_4
              SMC_StoreHighByte draw_sprite::scrw_4
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_5
              SMC_StoreHighByte draw_sprite::scrw_5
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_6
              SMC_StoreHighByte draw_sprite::scrw_6
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_7
              SMC_StoreHighByte draw_sprite::scrw_7
              lda     vl_high2, Y
              adc     ZPAGE
              SMC_StoreHighByte draw_sprite::scrr_8
              SMC_StoreHighByte draw_sprite::scrw_8
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_9
              SMC_StoreHighByte draw_sprite::scrw_9
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_a
              SMC_StoreHighByte draw_sprite::scrw_a
              adc     #$04
              SMC_StoreHighByte draw_sprite::scrr_b
              SMC_StoreHighByte draw_sprite::scrw_b
              rts
.endproc


; ----------------------------------------------------------------------------
; render_mario
; ----------------------------------------------------------------------------
.proc render_mario
              lda     page_shown      ; double buffering
              ldx     #>HGRPAGE2      ; drawing in the not diplayed screen
              eor     #<LOWSCR
              beq     draw            ; if A == <LOWSCR
              ldx     #>HGRPAGE1
draw:         stx     ZPAGE
              lda     #12
              sta     ZSPRITEY
              lda     #32
              sta     ZSPRITEX
              ldx     mario_spr
              lda     mario_seq, X
              sta     ZSPRITEID
              jsr     render_sprite
              ldx     mario_spr
              inx
              cpx     #3
              bne     end
              ldx     #0
end:          stx     mario_spr
              rts
.endproc


; ----------------------------------------------------------------------------
; render_sprite
; ----------------------------------------------------------------------------
.proc render_sprite
              ldy     ZSPRITEY
              lda     mult_17, Y
              tax
              lda     SCENE, X
              bpl     no_store
              ; mark tile for redraw
no_store:     ldx     #4
              jsr     set_sprite_line
              lda     ZSPRITEX
              and     #%00000011
              tax
              lda     sprite_set, X
              jsr     set_sprite_set
              lda     ZSPRITEX
              clc
              ror
              tax
              ldy     ZSPRITEID
              jmp     draw_sprite
.endproc


; ----------------------------------------------------------------------------
; render
; ----------------------------------------------------------------------------
.proc render
              lda     page_shown      ; double buffering
              ldx     #>HGRPAGE2      ; drawing in the not diplayed screen
              eor     #<LOWSCR
              beq     draw            ; if A == <LOWSCR
              ldx     #>HGRPAGE1
draw:         stx     ZPAGE
              ldx     ZPIXELSHIFT     ; Select texture set
              lda     texture_set, X
              jsr     set_texture_set
              ldy     #14
next_line:    sty     ZVAL2
              lda     mult_17, Y
              SMC_StoreLowByte render::scene_line
              SMC_StoreLowByte render::scene_line_l
              SMC_StoreLowByte render::scene_line_r
              ldx     ZPIXELSHIFT
              jsr     set_video_line

              lda     ZPIXELSHIFT
              ror
              ldx     #15
next_tile:    SMC     scene_line, { ldy SCENE, X } ; get texture code
              bpl     draw_it
              dex
              bne     next_tile
              bra     leftside
draw_it:      stx     ZVAL1
              lda     mult_2, X
              tax
              jsr     draw_tile
              ldx     ZVAL1
              dex
              bne     next_tile
              bra     leftside

rightside:    ldx     #16
              SMC     scene_line_r, { ldy SCENE, X } ; get texture code
              bmi     continue
              ldx     ZPIXELSHIFT
              lda     mult_2, X
              tax
              jmp     (rside_table, X)
rside1:       ldx     #32
              jsr     draw_tile::draw
              bra     continue
rside2:       ldx     #32
              stz     RAMWRTON
              jsr     draw_tile::draw
              stz     RAMWRTOFF
              iny
              jsr     draw_tile::draw
              bra     continue
rside3:       ldx     #32
              jsr     draw_tile::draw
              inx
              jsr     draw_tile::draw
              stz     RAMWRTON
              jsr     draw_tile::draw
              stz     RAMWRTOFF
              bra     continue
rside0:       ldx     #32
              jsr     draw_tile

continue:     ldy     ZVAL2
              dey
              bpl     next_line
              rts

leftside:     SMC     scene_line_l, { ldy SCENE, X } ; get texture code
              bmi     rightside
              ldx     ZPIXELSHIFT
              lda     mult_2, X
              tax
              jmp     (lside_table, X)
lside1:       ldx     #0
              iny
              jsr     draw_tile::last_3_s
              bra     rightside
lside2:       ldx     #1
              iny
              stz     RAMWRTON
              jsr     draw_tile::draw
              stz     RAMWRTOFF
              iny
              jsr     draw_tile::draw
              bra     rightside
lside3:       ldx     #2
              iny
              iny
              iny
              stz     RAMWRTON
              jsr     draw_tile::draw
              stz     RAMWRTOFF
              bra     rightside

.endproc


; ----------------------------------------------------------------------------
; read-only data
; ----------------------------------------------------------------------------
lside_table:  .addr   render::rightside, render::lside1
              .addr   render::lside2, render::lside3

rside_table:   .addr   render::rside0, render::rside1
              .addr   render::rside2, render::rside3

texture_set:  .byte   >TEXTURES + $00, >TEXTURES + $06
              .byte   >TEXTURES + $0c, >TEXTURES + $12

sprite_set:   .byte   >SPRITES + $00, >SPRITES + $12
              .byte   >SPRITES + $0c, >SPRITES + $06

pixel_shift:  .byte   0, 1, 1, 0, 2

mult_17:      .byte   0, 17, 34, 51, 68, 85, 102, 119
              .byte   136, 153, 170, 187, 204, 221, 238

mult_2:       .byte   0, 2, 4, 6, 8, 10, 12, 14
              .byte   16, 18, 20, 22, 24, 26, 28, 30, 32

mario_spr:    .byte  $00
mario_seq:    .byte  $04, $08, $0c
.include  "video_lines.inc"

; ----------------------------------------------------------------------------
; read-write data
; ----------------------------------------------------------------------------
page_shown:   .byte   <LOWSCR

