# Super Mario Bros IIc

An attempt to make a Super-Mario-Bros-like game on an Apple II in DHGR mode

Currently, it is only a tile arrangement scrolling by 1.75 pixel per frame.
Use any key to scroll.

The disk image I generate uses the DOS 3.3 layout but is not a DOS 3.3 disk.
The program loads data directly from sectors of the disk.

## Requirements
- cc65
  https://www.cc65.org/
- make
- a C compiler for the host to compile utilities

## Building
Just type:
`make`

## Enjoying
Use the mario.do disk image on your favourite emulator or on real hardware.

## To do
- make it faster
- make sprites transparent
- ...

