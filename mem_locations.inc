; Copyright (C) 2021 Jean-Noël Vittaut aka Obscoz
;
; This file is part of Super-Mario-Bros-IIc.
;
; Super-Mario-Bros-IIc is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Super-Mario-Bros-IIc is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY ; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Super-Mario-Bros-IIc.  If not, see <http://www.gnu.org/licenses/>


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; zero page
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ZVAL1         :=      $00
ZVAL2         :=      $01
ZVAL3         :=      $02
ZPTR1         :=      $06
ZPTR2         :=      $08
ZPTR3         :=      $0a
ZPAGE         :=      $10
ZSPRITEX      :=      $11
ZSPRITEY      :=      $12
ZSPRITEDY     :=      $13
ZSPRITEID     :=      $14
ZPIXELSHIFT   :=      $15
ZLEVELPOS     :=      $20 ; and $21

; $42-$43: used by rtws

; $F0 to $FF: used by rtws

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ADR_BRK       :=      $3f0
ADR_RESET     :=      $3f2
JMP_NMI       :=      $3fb
ADR_IRQ       :=      $3fe



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; soft switches
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RAMRW_D000B1  :=      $c08b           ; read and write RAM in $d000-$ffff
SET80MODE     :=      $c00d           ; turn on 80 columns
RAMRDOFF      :=      $c002           ; turn off RAMRD
RAMRDON       :=      $c003           ; turn on RAMRD
RAMWRTOFF     :=      $c004           ; turn off RAMWRT
RAMWRTON      :=      $c005           ; turn on RAMWRT
VERTBLANK     :=      $c019           ; vertical blank
TXTCLR        :=      $c050           ; display graphics
MIXCLR        :=      $c052           ; disable 4 lines of text
LOWSCR        :=      $c054           ; page 1
HISCR         :=      $c055           ; page 2
HIRES         :=      $c057           ; hires graphics
DHIRESON      :=      $c05e           ; enable double-width graphics
DHIRESOFF     :=      $c05f           ; disable double-width graphics
CLR80COL      :=      $c000           ; disable 80 column store
KBD           :=      $c000           ; read keyboard
KBDSTRB       :=      $c010           ; clear keyboard strobe
VBLINTON      :=      $c05b           ; enable VBL interrupt
VSYNCIRF      :=      $c070           ; reset VSYNC IRF (Interrupt Flag)
IOUOFF        :=      $c078           ; disable IOU access
IOUON         :=      $c079           ; enable IOU access


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; other locations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HGRPAGE1      :=      $2000           ; dhgr page 1
HGRPAGE2      :=      $4000           ; dhgr page 2

TEXTURES      :=      $6000           ; textures
SPRITES       :=      $8000           ; sprites
LEVEL         :=      $a000           ; level
MARIO         :=      $d000           ; game
SCENE         :=      $400            ; tile arrangement
SPRLIST       :=      $500            ; sprite arrangement

