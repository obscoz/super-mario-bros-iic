; from http://boutillon.free.fr/Underground/Outils/Beautiful_Boot/Txt/Boot2_Beautiful_Boot.txt

;********************************
;*                              *
;*         Mini RWTS            *
;*       by The Stack           *
;*  (c)1982 Corrupt Computing   *
;*                              *
;********************************

;* Notes (Deckard):
;*
;* Contains parts of the original DOS 3.3 RWTS
;* (c)1980 by Apple Computer Inc
;*
;* In fact MINI RWTS should be called MINI RTS
;* because it is a read only routine (no "W").


;* In: TRACK/SECTOR/NB_SECT/PTR_BUFF (low & high)

.include "mini_rwts.inc"

.org MINI_RWTS

              LDA    PARM_SD   ; get slot# * 16
              AND    #%01110000
              TAX              ; in X
              LDA    PARM_SD   ; get drive#
              AND    #%00000011
              BEQ    HBA1F     ; use last used drive

              LSR              ; [1,2] -> [0,1]
              BCC    HBA13     ; drive 2

              LDA    $C08A,X   ; drive 1
              BCS    HBA1F

HBA13:        LDA    $C08B,X   ; drive 2
              BCC    HBA1F

MINI_RWTS2:   LDX    #$60      ; set slot 6 *16
              NOP              ; LDA CURTRK
              NOP
              NOP
              NOP              ; STA ZCURTRK
              NOP

HBA1F:        LDA    NB_SECT
              BNE    SAMESLOT

              RTS              ; not a normal end

;*-------------------------------

SAMESLOT:     LDA    $C08E,X   ; start the motor up
              LDA    $C08E,X
              LDY    #8
HBA2C:        LDA    $C08C,X
              PHA
              PLA
              PHA
              PLA
              CMP    $C08C,X
              BNE    HBA3B

              DEY
              BNE    HBA2C

HBA3B:        PHP
              LDA    $C089,X
                               ; set parameters (device characteristics table)
                               ; motor on time count (DCT)
              LDA    #$D8      ; normal values (see your dos manual, RWTS
              STA    MONTIMEH  ; section, IOB table)
              LDA    #$EF      ;
              STA    MONTIMEL
              PLP
              PHP
              BNE    NOWAIT

              LDY    #8

STEPWAIT:     JSR    MSWAIT    ; wait 100 usec for old drive's timing
              DEY              ; capacitor to discharge
              BNE    STEPWAIT

NOWAIT:       LDA    TRACK
              JSR    MYSEEK
              PLP
              BNE    TRYTRK

              LDY    MONTIMEH
              BPL    TRYTRK

MOTOROFF:     LDY    #$12      ; wait for motor to come up to speed
HBA61:        DEY
              BNE    HBA61

              INC    MONTIMEL
              BNE    MOTOROFF

              INC    MONTIMEH
              BNE    MOTOROFF
                               ; disk is now up to speed

;*-------------------------------
;* Read address field
;* Same routine as DOS 3.3 RWTS
;*-------------------------------

TRYTRK:       LDY    #48       ; set up for a maximum of 48 retries
              STY    RETRYCNT

RDADR16:      LDY    #$FC      ; a too big gap is not allowed
              STY    COUNT_NIBBLE ; if D5 AA 96 not found before a raisonnable
HBA74:        INY              ; gap -> recalibration
              BNE    HBA7B

              INC    COUNT_NIBBLE
              BEQ    RDERR

;* Search for prologue

HBA7B:        LDA    $C08C,X
              BPL    HBA7B

HBA80:        CMP    #$D5      ; prologue D5
              BNE    HBA74

              NOP

HBA85:        LDA    $C08C,X
              BPL    HBA85

              CMP    #$AA      ; prologue AA
              BNE    HBA80

              LDY    #3        ; index ready for addr field informations
HBA90:        LDA    $C08C,X
              BPL    HBA90

              CMP    #$96      ; prologue 96
              BNE    HBA80

;* Search for sector informations

              LDA    #0        ; read volume/track/sector/checksum (4+4)
HBA9B:        STA    WCHKSUM   ; init working checksum

HBA9D:        LDA    $C08C,X   ; 1st nibble ('odd')
              BPL    HBA9D

              ROL
              STA    READ_CHKSUM
HBAA5:        LDA    $C08C,X   ; 2nd nibble ('even')
              BPL    HBAA5

              AND    READ_CHKSUM ; merge the two
              STA    $00F3,Y   ; store it
              EOR    WCHKSUM   ; update working checksum
              DEY
              BPL    HBA9B

              TAY              ; last read=checksum
              BNE    RDERR     ; bad checksum -> retry

;* Search for epilogue

HBAB7:        LDA    $C08C,X
              BPL    HBAB7

              CMP    #$DE      ; epilogue DE
              BNE    RDERR

              NOP

HBAC1:        LDA    $C08C,X
              BPL    HBAC1

              CMP    #$AA      ; epilogue AA
              BEQ    RDRIGHT   ; sector OK

;*-------------------------------

RDERR:        DEC    RETRYCNT  ; -1 retry
              BPL    RDADR16   ; not the last one

;*-------------------------------

;* Unable to find a correct address field
;* Recalibrate disk head

RECAL:        LDA    ZCURTRK   ; save track we want
              PHA
              LDA    #$60      ; not a real track but ok to force recalibration
              STA    ZCURTRK
              LDA    #0        ; track $00 please!
              JSR    MYSEEK
              PLA              ; and now go to the desired track
HBADB:        JSR    MYSEEK
              CLC              ; always
              BCC    TRYTRK    ; read address field under the R/W head

;*-------------------------------

;* We have just read an address field
;* Now check for desired track and sector

RDRIGHT:      LDY    READ_TRK  ; track found
              CPY    ZCURTRK   ; wanted track?
              BEQ    HBAEE     ; yes, found

              LDA    ZCURTRK   ; recalibrate on this
              STY    ZCURTRK   ; current track = track found
              CLC
              BCC    HBADB     ; always: exec recalibration

HBAEE:        LDY    SECTOR
              LDA    SKEWING,Y ; convert to "soft" sector (interleave)
              CMP    READ_SECT ; sector found?
              BNE    RDERR     ; no

;*-------------------------------
;* Read data field
;*-------------------------------

READ16:       LDY    #$20      ; a too big gap 2 is not allowed
RSYNC:        DEY              ; sync byte-1
              BEQ    RDERR     ; too big: retry

;* Read prologue

HBAFC:        LDA    $C08C,X
              BPL    HBAFC

HBB01:        EOR    #$D5      ; prologue D5
              BNE    RSYNC

              NOP

HBB06:        LDA    $C08C,X
              BPL    HBB06

              CMP    #$AA      ; prologue AA
              BNE    HBB01

              LDY    #$56      ; index part 1
HBB11:        LDA    $C08C,X
              BPL    HBB11

              CMP    #$AD      ; prologue AD
              BNE    HBB01

;* Read 342 nibbles (342=$156 => $56+$100)
;* = the data from the sector

              LDA    #0        ; init checksum

;* Part 1 : $56 nibbles

RDATA1:       DEY              ; read stuff into NBUF2
              STY    IDX

HBB1F:        LDY    $C08C,X
              BPL    HBB1F
                               ; Y[$96,$FF]
              EOR    RD_TRANS_TBL-$96,Y
              LDY    IDX       ; Y[$00,$55]
              STA    NBUF2,Y
              BNE    RDATA1

;* Part 2 : $100 nibbles

HBB2E:        STY    IDX       ; read stuff into NBUF1 (= final page)

HBB30:        LDY    $C08C,X
              BPL    HBB30
                               ; Y[$96,$FF]
              EOR    RD_TRANS_TBL-$96,Y
              LDY    IDX
              STA    (PTR_BUFFL),Y
              INY
              BNE    HBB2E

;* Checksum

HBB3F:        LDY    $C08C,X   ; read it
              BPL    HBB3F
                               ; Y[$96,$FF]
              CMP    RD_TRANS_TBL-$96,Y ; and compare (must be = 0)
RDERR2:       BNE    RDERR     ; bad sector -> retry

;* Read epilogue

HBB49:        LDA    $C08C,X
              BPL    HBB49

              CMP    #$DE      ; epilogue DE
              BNE    RDERR2    ; no -> bad sector: retry

              NOP

HBB53:        LDA    $C08C,X
              BPL    HBB53

              CMP    #$AA      ; epilogue AA
              BNE    RDERR2    ; no -> bad sector: retry

              STX    SLOTEMP

;* Post nibblize routine

              LDY    #0
HBB60:        LDX    #$56
HBB62:        DEX
              BMI    HBB60

              LDA    (PTR_BUFFL),Y
              LSR    NBUF2,X
              ROL
              LSR    NBUF2,X
              ROL
              STA    (PTR_BUFFL),Y ; store in user buffer
              INY
              BNE    HBB62

;*-------------------------------

;* Prepare mini RWTS' parms if more than 1 sector to load

              LDX    SLOTEMP
              LDA    $C088,X
              INC    PTR_BUFFH ; next page of memory
              LDY    SECTOR    ; previous sector
              DEY
              BPL    HBB84     ; was not sector $00

              LDY    #$0F      ; next sector = $0F of the previous track
              DEC    TRACK

HBB84:        STY    SECTOR
              DEC    NB_SECT   ; nbr sector-1. Was last sector?
              BEQ    HBB95     ; yes, end.

              JMP    HBA3B     ; read the next one

;*-------------------------------
;* Seek track routine
;* (uses SEEKABS)
;*-------------------------------

;* In: Acc = desired track
;*     X   = slot * 16

MYSEEK:       ASL
              ASL    ZCURTRK   ; half track
              JSR    SEEKABS
              LSR    ZCURTRK   ; full track
HBB95:        RTS

;*-------------------------------
;* SEEKABS routine, moves the
;* disk head over the desired
;* track (same as DOS 3.3 RWTS)
;*-------------------------------

;* In : Acc = desired half track
;*      X   = slot * 16

SEEKABS:      STX    SLOTEMP
              STA    TRKN
              CMP    DRV0TRK   ; current half track?
              BEQ    HBBED     ; yep: end

              LDA    #0
              STA    TRKCNT
MOVEHEAD:     LDA    DRV0TRK
              STA    PRIOR
              SEC
              SBC    TRKN
              BEQ    ISTHERE

              BCS    HBBB3

              EOR    #$FF
              INC    DRV0TRK
              BCC    HBBB7

HBBB3:        ADC    #$FE
              DEC    DRV0TRK
HBBB7:        CMP    TRKCNT
              BCC    HBBBD

              LDA    TRKCNT
HBBBD:        CMP    #$0C
              BCS    HBBC2

              TAY
HBBC2:        SEC
              JSR    CHKPOS
              LDA    ONTBL,Y
              JSR    MSWAIT
              LDA    PRIOR
              CLC
              JSR    CHKPOS2
              LDA    OFFTBL,Y
              JSR    MSWAIT
              INC    TRKCNT
              BNE    MOVEHEAD

;*-------------------------------

ISTHERE:      JSR    MSWAIT    ; wait arm
              CLC
CHKPOS:       LDA    DRV0TRK
CHKPOS2:      AND    #%00000011
              ROL
              ORA    SLOTEMP
              TAX
              LDA    $C080,X
              LDX    SLOTEMP
HBBED:        RTS

              .byt   0
              .byt   0

SKEWING:      .byt   $00, $0D, $0B, $09, $07, $05, $03, $01, $0E, $0C, $0A
              .byt   $08, $06, $04, $02, $0F


;*-------------------------------
;* Head move delay subroutine
;* Delay acc*100+ usec
;* (Same as DOS 3.3 RWTS)
;*-------------------------------

MSWAIT:       STX    WTEMP     ; added

HBC02:        LDX    #$11
HBC04:        DEX
              BNE    HBC04

              INC    MONTIMEL
              BNE    HBC0D

              INC    MONTIMEH

HBC0D:        SEC
              SBC    #1
              BNE    HBC02

              LDX    WTEMP     ; added
              RTS

;*-------------------------------
;* Phaseon/phaseoff tables
;*-------------------------------

ONTBL:        .byt   $01, $30, $28, $24, $20, $1E, $1D, $1C

              .byt   $1C, $1C, $1C, $1C


OFFTBL:       .byt   $70, $2C, $26, $22

              .byt   $1F, $1E, $1D, $1C, $1C, $1C, $1C, $1C


;*-------------------------------
;* Read translate table
;*-------------------------------

;* DOS 3.3 : BA96.BAFF

RD_TRANS_TBL: .byt   $00, $01, $98, $99, $02, $03, $9C, $04

              .byt   $05, $06, $A0, $A1, $A2, $A3, $A4, $A5

              .byt   $07, $08, $A8, $A9, $AA, $09, $0A, $0B

              .byt   $0C, $0D, $B0, $B1, $0E, $0F, $10, $11

              .byt   $12, $13, $B8, $14, $15, $16, $17, $18

              .byt   $19, $1A, $C0, $C1, $C2, $C3, $C4, $C5

              .byt   $C6, $C7, $C8, $C9, $CA, $1B, $CC, $1C

              .byt   $1D, $1E, $D0, $D1, $D2, $1F, $D4, $D5

              .byt   $20, $21, $D8, $22, $23, $24, $25, $26

              .byt   $27, $28, $E0, $E1, $E2, $E3, $E4, $29

              .byt   $2A, $2B, $E8, $2C, $2D, $2E, $2F, $30

              .byt   $31, $32, $F0, $F1, $33, $34, $35, $36

              .byt   $37, $38, $F8, $39, $3A, $3B, $3C, $3D

              .byt   $3E, $3F

NBUF2:        .res   $56, 0    ; $56 bytes buffer
