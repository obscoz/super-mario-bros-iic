MINI_RWTS     :=      $fd00

;*-------------------------------
;* Zero page
;*-------------------------------

PARM_SD       :=     $F1       ; [0sss00dd] s=slot, d=drive
                               ; if dd=0, don't use drive number switch
                               ; = use last used drive

;* DOS 3.3 File Manager

BUFADRL       :=     $42       ; file buffer (low)
BUFADRH       :=     $43       ; (high)

;* RDADR16 routine

RETRYCNT      :=     $F2       ; nbr of retries
COUNT_NIBBLE  :=     $F5       ; max nbr of nibbles before D5 AA 96
WTEMP         :=     $F6       ; temp. field
WCHKSUM       :=     $F8       ; working checksum

;* Found address field

READ_CHKSUM   :=     $F3       ; checksum
READ_SECT     :=     $F4       ; sector number
READ_TRK      :=     $F5       ; track number
READ_VOL      :=     $F6       ; volume number

;* Read data field

IDX           :=     $F6       ; index

;* MYSEEK/SEEKABS (DOS 3.3 RWTS)

TRKCNT        :=     $F3
PRIOR         :=     $F4
TRKN          :=     $F5
DRV0TRK       :=     $F7
ZCURTRK       :=     $F7
SLOTEMP       :=     $F8
MONTIMEL      :=     $F9
MONTIMEH      :=     $FA

;* Mini RWTS parms

PTR_BUFFL     :=     $FB       ; pointer (low) for addr where to load pgm
PTR_BUFFH     :=     $FC       ; (high)
NB_SECT       :=     $FD       ; number of sectors
TRACK         :=     $FE       ; start track
SECTOR        :=     $FF       ; start sector


