default: mario.do

non-free/mini_rwts.bin: non-free/mini_rwts.s non-free/mini_rwts.inc
	ca65 non-free/mini_rwts.s
	ld65 -C simple_binary.cfg non-free/mini_rwts.o -o non-free/mini_rwts.bin

utilities/make_textures: utilities/make_textures.c
	gcc -O2 -Wall -o utilities/make_textures utilities/make_textures.c

utilities/make_sprites: utilities/make_sprites.c
	gcc -O2 -Wall -o utilities/make_sprites utilities/make_sprites.c

utilities/make_level: utilities/make_level.c
	gcc -O2 -Wall -o utilities/make_level utilities/make_level.c

utilities/make_disk: utilities/make_disk.c
	gcc -O2 -Wall -o utilities/make_disk utilities/make_disk.c

utilities/make_video_lines: utilities/make_video_lines.c
	gcc -O2 -Wall -o utilities/make_video_lines utilities/make_video_lines.c

bootloader.bin: bootloader.s mem_locations.inc
	ca65 bootloader.s
	ld65 -C simple_binary.cfg bootloader.o -o bootloader.bin

mario.bin: mario.s mem_locations.inc video_lines.inc
	ca65 mario.s
	ld65 -C simple_binary.cfg mario.o -o mario.bin

textures.bin: textures.dat utilities/make_textures
	utilities/make_textures <textures.dat >textures.bin

sprites.bin: sprites.dat utilities/make_sprites
	utilities/make_sprites <sprites.dat >sprites.bin

level1.bin: level1.dat utilities/make_level
	utilities/make_level < level1.dat >level1.bin

video_lines.inc: utilities/make_video_lines
	utilities/make_video_lines >video_lines.inc

mario.do: mario.bin bootloader.bin textures.bin sprites.bin level1.bin utilities/make_disk disk_layout.def non-free/mini_rwts.bin
	utilities/make_disk <disk_layout.def >mario.do

clean:
	rm -f non-free/mini_rwts.o non-free/mini_rwts.bin
	rm -f utilities/make_disk utilities/make_level
	rm -f utilities/make_textures utilities/make_sprites
	rm -f utilities/make_video_lines
	rm -f bootloader.o bootloader.bin
	rm -f mario.o mario.bin
	rm -f textures.bin sprites.bin level1.bin
	rm -f mario.do

